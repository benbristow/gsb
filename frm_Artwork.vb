﻿Public Class frm_Artwork

    Private Sub RefreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Artwork")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Artwork")
        DataGridView1.ClearSelection()
    End Sub

    Private Sub frm_Artwork_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplitContainer1.Panel1Collapsed = True

        'Update combo boxes.
        DatabaseManager.RunSelectAllOnTable("tbl_Specifications")
        For Each spec As DataRow In DatabaseManager.ds.Tables("tbl_Specifications").Rows
            cb_Specification.Items.Add(spec("SpecID") & ": " & spec("Title"))
        Next

        DatabaseManager.RunSelectAllOnTable("tbl_Staff")
        For Each employee As DataRow In DatabaseManager.ds.Tables("tbl_Staff").Rows
            cb_Designer.Items.Add(employee("StaffID") & ": " & employee("FirstName") & " " & employee("LastName"))
        Next

        DatabaseManager.RunSelectAllOnTable("tbl_PrintingMethod")
        For Each method As DataRow In DatabaseManager.ds.Tables("tbl_PrintingMethod").Rows
            cb_PrintingMethod.Items.Add(method("PMethodID") & ": " & method("Title"))
        Next

        RefreshDataView()
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
    End Sub

    'Add panel
    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation
        If (cb_Specification.SelectedItem = "") Then
            MessageBox.Show("No specification selected")
            Return
        End If

        If (cb_Designer.SelectedItem = "") Then
            MessageBox.Show("No Designer Selected")
            Return
        End If

        If (cb_PrintingMethod.SelectedItem = "") Then
            MessageBox.Show("No printing method selected")
            Return
        End If

        'Save new artwork
        DatabaseManager.RunSelectAllOnTable("tbl_Artwork")
        Dim newArtwork As DataRow = DatabaseManager.ds.Tables("tbl_Artwork").NewRow()

        newArtwork("ArtworkID") = DatabaseManager.getNextID("tbl_Artwork")
        newArtwork("Designer") = cb_Designer.SelectedItem.Remove(cb_Designer.SelectedItem.IndexOf(":"))
        newArtwork("SpecID") = cb_Specification.SelectedItem.Remove(cb_Specification.SelectedItem.IndexOf(":"))
        newArtwork("PrintingMethod") = cb_PrintingMethod.SelectedItem.Remove(cb_PrintingMethod.SelectedItem.IndexOf(":"))
        newArtwork("Instructions") = txt_Instructions.Text

        DatabaseManager.Update(newArtwork, "tbl_Artwork")

        'Update GUI
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
        RefreshDataView()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    'Bottom options
    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub


    Private Sub btn_Edit_Click(sender As Object, e As EventArgs)
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub


    Private Sub btn_Approve_Click(sender As Object, e As EventArgs) Handles btn_Approve.Click
        'Approval
        If (DataGridView1.SelectedRows.Count = 0) Then
            MessageBox.Show("No rows selected")
            Return
        End If

        Dim indicies As New List(Of String)
        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.RunSQL(String.Format("UPDATE tbl_Artwork SET ApprovalDate='{0}' WHERE ArtworkID='{1}';", Date.Now.ToString(), index), "tbl_Artwork")
        Next

        RefreshDataView()
    End Sub

    Private Sub btn_MarkAsComplete_Click(sender As Object, e As EventArgs) Handles btn_MarkAsComplete.Click
        'Complete
        If (DataGridView1.SelectedRows.Count = 0) Then
            MessageBox.Show("No rows selected")
            Return
        End If

        Dim indicies As New List(Of String)
        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.RunSQL(String.Format("UPDATE tbl_Artwork SET DateComplete='{0}' WHERE ArtworkID='{1}';", Date.Now.ToString(), index), "tbl_Artwork")
        Next

        RefreshDataView()
    End Sub

    Private Sub clearInputs()
        cb_Specification.SelectedItem = ""
        cb_Designer.SelectedItem = ""
        cb_PrintingMethod.SelectedItem = ""
        txt_Instructions.Clear()
    End Sub

    Private Sub toggleButtons()
        If (btn_Add.Enabled = True) Then
            btn_Add.Enabled = False
            btn_Delete.Enabled = False
            btn_MarkAsComplete.Enabled = False
            btn_Approve.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Delete.Enabled = True
            btn_MarkAsComplete.Enabled = True
            btn_Approve.Enabled = True
        End If
    End Sub


    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Artwork", index)
        Next

        RefreshDataView()
    End Sub
End Class