﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_BackupRestore
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_BackupRestore))
        Me.btn_Backup = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_BackupBrowse = New System.Windows.Forms.Button()
        Me.txt_BackupFile = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_RestoreBrowse = New System.Windows.Forms.Button()
        Me.txt_RestoreFile = New System.Windows.Forms.TextBox()
        Me.btn_Restore = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_Backup
        '
        Me.btn_Backup.Image = CType(resources.GetObject("btn_Backup.Image"), System.Drawing.Image)
        Me.btn_Backup.Location = New System.Drawing.Point(373, 45)
        Me.btn_Backup.Name = "btn_Backup"
        Me.btn_Backup.Size = New System.Drawing.Size(73, 24)
        Me.btn_Backup.TabIndex = 0
        Me.btn_Backup.Text = "Backup"
        Me.btn_Backup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_Backup.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_BackupBrowse)
        Me.GroupBox1.Controls.Add(Me.txt_BackupFile)
        Me.GroupBox1.Controls.Add(Me.btn_Backup)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(452, 75)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Backup Data to File"
        '
        'btn_BackupBrowse
        '
        Me.btn_BackupBrowse.Location = New System.Drawing.Point(415, 18)
        Me.btn_BackupBrowse.Name = "btn_BackupBrowse"
        Me.btn_BackupBrowse.Size = New System.Drawing.Size(31, 21)
        Me.btn_BackupBrowse.TabIndex = 2
        Me.btn_BackupBrowse.Text = "..."
        Me.btn_BackupBrowse.UseVisualStyleBackColor = True
        '
        'txt_BackupFile
        '
        Me.txt_BackupFile.Location = New System.Drawing.Point(6, 19)
        Me.txt_BackupFile.Name = "txt_BackupFile"
        Me.txt_BackupFile.ReadOnly = True
        Me.txt_BackupFile.Size = New System.Drawing.Size(409, 20)
        Me.txt_BackupFile.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_RestoreBrowse)
        Me.GroupBox2.Controls.Add(Me.txt_RestoreFile)
        Me.GroupBox2.Controls.Add(Me.btn_Restore)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 93)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(452, 75)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Restore Data from File"
        '
        'btn_RestoreBrowse
        '
        Me.btn_RestoreBrowse.Location = New System.Drawing.Point(415, 18)
        Me.btn_RestoreBrowse.Name = "btn_RestoreBrowse"
        Me.btn_RestoreBrowse.Size = New System.Drawing.Size(31, 20)
        Me.btn_RestoreBrowse.TabIndex = 2
        Me.btn_RestoreBrowse.Text = "..."
        Me.btn_RestoreBrowse.UseVisualStyleBackColor = True
        '
        'txt_RestoreFile
        '
        Me.txt_RestoreFile.Location = New System.Drawing.Point(6, 19)
        Me.txt_RestoreFile.Name = "txt_RestoreFile"
        Me.txt_RestoreFile.ReadOnly = True
        Me.txt_RestoreFile.Size = New System.Drawing.Size(409, 20)
        Me.txt_RestoreFile.TabIndex = 1
        '
        'btn_Restore
        '
        Me.btn_Restore.Image = CType(resources.GetObject("btn_Restore.Image"), System.Drawing.Image)
        Me.btn_Restore.Location = New System.Drawing.Point(373, 45)
        Me.btn_Restore.Name = "btn_Restore"
        Me.btn_Restore.Size = New System.Drawing.Size(73, 24)
        Me.btn_Restore.TabIndex = 0
        Me.btn_Restore.Text = "Restore"
        Me.btn_Restore.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_Restore.UseVisualStyleBackColor = True
        '
        'frm_BackupRestore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(476, 179)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frm_BackupRestore"
        Me.Text = "Backup/Restore"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_Backup As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_BackupBrowse As System.Windows.Forms.Button
    Friend WithEvents txt_BackupFile As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_RestoreBrowse As System.Windows.Forms.Button
    Friend WithEvents txt_RestoreFile As System.Windows.Forms.TextBox
    Friend WithEvents btn_Restore As System.Windows.Forms.Button
End Class
