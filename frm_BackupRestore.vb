﻿'Dependencies.

'Ionic Zip required for Compressing/Decompressing ZIP files.
Imports Ionic.Zip

'Required by .NET for file operations.
Imports System.IO

Public Class frm_BackupRestore

    Private Sub btn_BackupBrowse_Click(sender As Object, e As EventArgs) Handles btn_BackupBrowse.Click
        'Create a save file dialog.
        Dim browser As New SaveFileDialog

        'Settings for save file dialog.
        browser.Title = "Choose Backup File Destination"
        browser.AddExtension = True
        browser.DefaultExt = "gsbbak"

        'Default file name from current time/date. Can be changed.
        browser.FileName = "gsb_Backup-" & Date.Now.ToFileTime

        'Add a filter so only gsbbak files can be selected.
        browser.Filter = "GSB Backup File|*.gsbbak"

        'Show dialog.
        browser.ShowDialog()

        'Set text box with backup file name to be the selected file name.
        txt_BackupFile.Text = browser.FileName
    End Sub

    Private Sub btn_RestoreBrowse_Click(sender As Object, e As EventArgs) Handles btn_RestoreBrowse.Click
        'Create an open file dialog.
        Dim browser As New OpenFileDialog

        'Add a title and a filter so only gsbbak files can be selected.
        browser.Title = "Choose Backup File Destination"
        browser.Filter = "GSB Backup File|*.gsbbak"

        'Show dialog to use.
        browser.ShowDialog()

        'Set text box with restore file name to the selected file name.
        txt_RestoreFile.Text = browser.FileName
    End Sub

    Private Sub btn_Backup_Click(sender As Object, e As EventArgs) Handles btn_Backup.Click
        'Has a file been chosen?
        If (txt_BackupFile.Text = "") Then
            'Nope, show an error and stop.
            MessageBox.Show("No backup file specified")
            Return
        End If

        'Create password input dialog.
        Dim passwordInput As New frm_PasswordInput("Enter password to encrypt file with", "System Backup")

        'Show dialog. Wait for response. If the user didn't select OK on the password dialog, stop.
        If (passwordInput.ShowDialog() <> Windows.Forms.DialogResult.OK) Then
            Return
        End If

        'Get password from password input form.
        Dim password As String = passwordInput.getPassword()

        'Check password is not less than 5 characters long. If it's less than 5 characters long, stop.
        If (password.Length < 7) Then
            MessageBox.Show("Password length can't be less than 7 characters long")
            Return
        End If

        'Create new Zip File with DotNetZip Library
        Dim zip As New ZipFile()

        'Encrypt ZIP with password.
        zip.Password = password

        'Select Encryption Algorithm to use. AES256 is strong.
        zip.Encryption = EncryptionAlgorithm.WinZipAes256

        'Add file to Zip
        zip.AddFile("GSB.mdb")

        'Save!
        zip.Save(txt_BackupFile.Text)

        'Show message box to use with success prompt.
        MessageBox.Show("Backup saved!")

        'Reset file selection textbox.
        txt_BackupFile.Text = ""
    End Sub

    Private Sub btn_Restore_Click(sender As Object, e As EventArgs) Handles btn_Restore.Click
        'Create a password input dialog.
        Dim passwordInput As New frm_PasswordInput("Enter the password that you encrypted this file with", "System Restore")

        'Show dialog. Wait for response. If the user didn't select OK on the password dialog, stop.
        If (passwordInput.ShowDialog() <> Windows.Forms.DialogResult.OK) Then
            Return
        End If

        'Get password from the password dialog.
        Dim password As String = passwordInput.getPassword()

        'Check restore file has been selected. If it hasn't, show error and stop.
        If (txt_RestoreFile.Text = "") Then
            MessageBox.Show("No restore file speciifed")
            Return
        End If

        'Check the restore file is actually in a ZIP file format and isn't trying to fool us.
        'If it isn't, show error and stop.
        If (Not ZipFile.IsZipFile(txt_RestoreFile.Text)) Then
            MessageBox.Show("Not a valid restore file.")
            Return
        End If

        'Check the password specified by the user matches the password of the ZIP file.
        'If it isn't, show error and stop.
        If (Not ZipFile.CheckZipPassword(txt_RestoreFile.Text, password)) Then
            MessageBox.Show("Invalid Password")
            Return
        End If

        'Create New Zip File Object
        Dim zip As ZipFile

        Try
            'Close Database Connection to prevent file-access errors.            
            DatabaseManager.CloseConnection()

            'Make temporary 'backup'
            File.Copy("GSB.mdb", "GSB.mdb.tmp")

            'Load ZIP file
            zip = ZipFile.Read(txt_RestoreFile.Text)

            'Use Password
            zip.Password = password

            'Decompress database from ZIP to current directory.
            zip.Entries(0).Extract(ExtractExistingFileAction.DoNotOverwrite)

            'Close Zip Reader to prevent file access errors
            zip.Dispose()

            'Delete backup.
            File.Delete("GSB.mdb.tmp")

            'Prompt And Exit
            MessageBox.Show("Restore successful. Please manually restart the application.")
            Application.Exit()
        Catch ex As Exception
            'Something went wrong. Let's fix this.

            'Restore the backup 
            File.Move("GSB.mdb.tmp", "GSB.mdb")

            'Reopen Database Connection
            DatabaseManager.OpenConnection()

            'Show error message
            MessageBox.Show("Something went wrong. Error: " & ex.Message)
        End Try
    End Sub
End Class