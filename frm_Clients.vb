﻿Public Class frm_Clients

    Sub refreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Client")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Client")
        DataGridView1.ClearSelection()
    End Sub

    Private Sub frm_Clients_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplitContainer1.Panel1Collapsed = True
        refreshDataView()
    End Sub

    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation
        If (txt_FirstName.Text.Length = 0 Or txt_LastName.Text.Length = 0 Or txt_Postcode.Text.Length = 0 Or txt_TelephoneNumber.Text.Length = 0 Or txt_AddressLine1.Text.Length = 0) Then
            MessageBox.Show("Not all required fields are complete")
            Return
        End If

        If Not (ValidatePostcode(txt_Postcode.Text)) Then
            MessageBox.Show("Invalid Postcode format. Please try again")
            Return
        End If

        If Not (ValidateTelephone(txt_TelephoneNumber.Text)) Then
            MessageBox.Show("Invalid Telephone number format. Check that it's a valid landline number and try again.")
            Return
        End If

        'Save new client
        DatabaseManager.RunSelectAllOnTable("tbl_Client")
        Dim newClient As DataRow = DatabaseManager.ds.Tables("tbl_Client").NewRow()

        newClient("ClientID") = DatabaseManager.getNextID("tbl_Client")
        newClient("FirstName") = txt_FirstName.Text
        newClient("LastName") = txt_LastName.Text
        newClient("CompanyName") = txt_CompanyName.Text
        newClient("AddressLine1") = txt_AddressLine1.Text
        newClient("Postcode") = txt_Postcode.Text
        newClient("FaxNo") = txt_FaxNumber.Text
        newClient("AccountNo") = txt_AccountNo.Text

        Dim passwordInput As New frm_PasswordInput("Enter a password for the client", "Client Password")
        If (passwordInput.ShowDialog() <> Windows.Forms.DialogResult.OK) Then
            Return
        End If

        Dim password As String = passwordInput.getPassword()
        If (password.Length < 7) Then
            MessageBox.Show("Password must be longer than 7 characters long")
            Return
        End If

        newClient("PasswordHash") = EncryptPassword(password)

        DatabaseManager.Update(newClient, "tbl_Client")

        'Update GUI
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
        refreshDataView()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub

    Private Sub clearInputs()
        txt_AddressLine1.Clear()
        txt_CompanyName.Clear()
        txt_FaxNumber.Clear()
        txt_FirstName.Clear()
        txt_LastName.Clear()
        txt_Postcode.Clear()
        txt_TelephoneNumber.Clear()
    End Sub

    Private Sub toggleButtons()
        If (btn_Add.Enabled = True) Then
            btn_Add.Enabled = False
            btn_Delete.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Delete.Enabled = True
        End If
    End Sub

    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Client", index)
        Next

        refreshDataView()
    End Sub
End Class