﻿Public Class frm_Deliveries

    Private Sub RefreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Delivery")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Delivery")
        DataGridView1.ClearSelection()
    End Sub

    Private Sub frm_Deliveries_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplitContainer1.Panel1Collapsed = True

        'Update combo boxes.
        DatabaseManager.RunSelectAllOnTable("tbl_Specifications")
        For Each spec As DataRow In DatabaseManager.ds.Tables("tbl_Specifications").Rows
            cb_Job.Items.Add(spec("SpecID") & ": " & spec("Title"))
        Next

        DatabaseManager.RunSelectAllOnTable("tbl_DeliveryMethod")
        For Each spec As DataRow In DatabaseManager.ds.Tables("tbl_DeliveryMethod").Rows
            cb_DeliveryMethod.Items.Add(spec("DMethodID") & ": " & spec("Description"))
        Next

        DatabaseManager.RunSelectAllOnTable("tbl_Staff")
        For Each spec As DataRow In DatabaseManager.ds.Tables("tbl_Staff").Rows
            cb_PackedBy.Items.Add(spec("StaffID") & ": " & spec("FirstName") & " " & spec("LastName"))
        Next

        RefreshDataView()
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
    End Sub

    'Add panel
    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation
        If (cb_Job.SelectedItem = "") Then
            MessageBox.Show("No specification selected")
            Return
        End If

        If (cb_DeliveryMethod.SelectedItem = "") Then
            MessageBox.Show("No delivery method selected")
            Return
        End If

        If (cb_PackedBy.SelectedItem = "") Then
            MessageBox.Show("No staff memeber specified")
            Return
        End If

        If (num_Boxes.Value < 0) Then
            MessageBox.Show("Number of boxes can not be less than 0")
        End If

        If (num_Weight.Value < 0) Then
            MessageBox.Show("Weight can not be less than 0kg!")
        End If

        'Save new artwork
        DatabaseManager.RunSelectAllOnTable("tbl_Delivery")
        Dim newDelivery As DataRow = DatabaseManager.ds.Tables("tbl_Delivery").NewRow()

        newDelivery("DeliveryID") = DatabaseManager.getNextID("tbl_Delivery")
        newDelivery("SpecID") = cb_Job.SelectedItem.Remove(cb_Job.SelectedItem.IndexOf(":"))
        newDelivery("Method") = cb_DeliveryMethod.SelectedItem.Remove(cb_DeliveryMethod.SelectedItem.IndexOf(":"))
        newDelivery("Weight") = num_Weight.Value.ToString()
        newDelivery("PackedBy") = cb_PackedBy.SelectedItem.Remove(cb_PackedBy.SelectedItem.IndexOf(":"))
        newDelivery("NoOfBoxes") = num_Boxes.Value.ToString()
        newDelivery("TrackingNumber") = txt_TrackingNo.Text
        newDelivery("Courier") = txt_Courier.Text

        DatabaseManager.Update(newDelivery, "tbl_Delivery")

        'Update GUI
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
        RefreshDataView()

    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    'Bottom options
    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub

    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Delivery", index)
        Next

        RefreshDataView()
    End Sub

    Private Sub btn_MarkAsDelivered_Click(sender As Object, e As EventArgs) Handles btn_MarkAsDelivered.Click
        'Mark As Delivered
        If (DataGridView1.SelectedRows.Count = 0) Then
            MessageBox.Show("No rows selected")
            Return
        End If

        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.RunSQL(String.Format("UPDATE tbl_Delivery SET DateDelivered='{0}' WHERE DeliveryID='{1}';", Date.Now.ToString(), index), "tbl_Delivery")
        Next

        RefreshDataView()
    End Sub

    Private Sub clearInputs()
        cb_DeliveryMethod.SelectedItem = ""
        cb_Job.SelectedItem = ""
        cb_PackedBy.SelectedText = ""
        num_Weight.Value = 0
        num_Boxes.Value = 0
        txt_Courier.Clear()
        txt_TrackingNo.Clear()
    End Sub

    Private Sub toggleButtons()
        If (btn_Add.Enabled = True) Then
            btn_Add.Enabled = False
            btn_Delete.Enabled = False
            btn_MarkAsDelivered.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Delete.Enabled = True
            btn_MarkAsDelivered.Enabled = True
        End If
    End Sub


End Class