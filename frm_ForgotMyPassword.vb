﻿Imports System.Net.Mail
Imports System.Net

Public Class frm_ForgotMyPassword

    Private Sub btn_Continue_Click(sender As Object, e As EventArgs) Handles btn_Continue.Click
        'Continue
        Try
            sendResetCodeEmail(txt_EmailAddress.Text)
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        'Cancel
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub sendResetCodeEmail(ByVal emailAddress As String)
        'Check email is valid
        Dim r As New RegexUtilities()
        If (Not r.IsValidEmail(txt_EmailAddress.Text)) Then
            Throw New Exception("Invalid email address format")
        End If

        'Check if email exists
        DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Staff WHERE Email='{0}';", txt_EmailAddress.Text), "tbl_Staff")

        If (DatabaseManager.ds.Tables("tbl_Staff").Rows.Count = 0) Then
            Throw New Exception("Email address does not match any staff on record")
        End If

        'Get Staff ID
        Dim StaffID As String = DatabaseManager.ds.Tables("tbl_Staff").Rows(0).Item("StaffID")

        'Generate Reset Code
        Dim resetCode As String = generateResetCode()

        'Update User Record
        DatabaseManager.RunSQL(String.Format("UPDATE tbl_Staff SET ResetCode='{0}' WHERE StaffID='{1}';", resetCode, StaffID), "tbl_Staff")

        'Setup & Send Email
        Dim SMTPclient As New SmtpClient
        Dim Email As New MailMessage()

        'Setup SMTP Client - This will need to be changed for Production use.
        SMTPclient.UseDefaultCredentials = False
        SMTPclient.Credentials = New Net.NetworkCredential("gsb@benbristow.co.uk", "gsbprinting")
        SMTPclient.Port = 465
        SMTPclient.EnableSsl = True
        SMTPclient.Host = "mail.benbristow.co.uk"

        'Compose Email
        Email = New MailMessage()
        Email.From = New MailAddress("gsb@benbristow.co.uk")
        Email.To.Add(txt_EmailAddress.Text)
        Email.Subject = "GSB Password Reset"
        Email.IsBodyHtml = True
        Email.Body = "<strong>Your password reset code is below. You should copy and paste this into the 'Reset Code' field.</strong> <br/><br/>" & resetCode

        'Send!
        SMTPclient.Send(Email)
    End Sub

    Private Function generateResetCode()
        Dim code As String = ""
        Dim rnd As New Random

        While (code.Length < 20)
            code += Chr(rnd.Next(65, 123)).ToString()
        End While

        Return code
    End Function
End Class