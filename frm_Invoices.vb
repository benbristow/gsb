﻿Public Class frm_Invoices
    Private session As Session

    Private Sub RefreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Invoice")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Invoice")
    End Sub

    Sub PopulateQuotesListBox()
        Try
            DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Quote WHERE ClientID='{0}' AND Reference NOT IN (SELECT QuoteReference FROM tbl_Job) AND DateAccepted IS NOT NULL AND DateDeclined IS NULL", cb_Client.SelectedItem.Remove(cb_Client.SelectedItem.IndexOf(":"))), "QuotesForClients")
            lb_Quote.Items.Clear()
            For Each quote As DataRow In DatabaseManager.ds.Tables("QuotesForClients").Rows
                lb_Quote.Items.Add(quote("Reference") & ": " & quote("QuoteDate") & " £" & quote("QuotedPrice"))
            Next
        Catch ex As NullReferenceException
            MessageBox.Show("No quotes currently in the system.")
            Me.Close()
        End Try
    End Sub

    Sub New(ByVal sess As Session)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.session = sess
    End Sub

    Private Sub frm_Invoices_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DatabaseManager.RunSelectAllOnTable("tbl_Client")
        For Each client As DataRow In DatabaseManager.ds.Tables("tbl_Client").Rows
            cb_Client.Items.Add(client("ClientID") & ": " & client("FirstName") & " " & client("LastName"))
        Next

        Try
            cb_Client.SelectedIndex = 0
        Catch
            'If there's no clients, the selectedindex setter will throw an exception. Handle this with an error and close the form.
            MessageBox.Show("You must have at least one client in the database to issue invoices")
            Me.Close()
        End Try

        RefreshDataView()
        SplitContainer1.Panel1Collapsed = True
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs)
    End Sub

    'Add panel
    Private Sub cb_Client_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_Client.SelectedIndexChanged
        PopulateQuotesListBox()
    End Sub

    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation
        If (num_SellPrice.Value < 0) Then
            MessageBox.Show("Can't be a negative sell price")
            Return
        End If

        If (lb_Quote.SelectedItems.Count = 0) Then
            MessageBox.Show("No quote selected")
            Return
        End If

        'Save new job
        DatabaseManager.RunSelectAllOnTable("tbl_Invoice")
        Dim newInvoice As DataRow = DatabaseManager.ds.Tables("tbl_Invoice").NewRow()

        newInvoice("InvoiceNumber") = DatabaseManager.getNextID("  ")
        newInvoice("InvoiceDate") = Date.Now.ToString
        newInvoice("QuoteReference") = lb_Quote.SelectedItem.Remove(lb_Quote.SelectedItem.IndexOf(":"))
        newInvoice("SellPrice") = num_SellPrice.Value.ToString()
        newInvoice("VATCode") = txt_VATCode.Text
        newInvoice("CurrencySymbol") = txt_Currency.Text
        newInvoice("ApprovedBy") = session.StaffID
        newInvoice("TotalPaid") = "0.00"        

        DatabaseManager.Update(newInvoice, "tbl_Invoice")

        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
        RefreshDataView()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    'Bottom options
    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub

    Private Sub btn_Update_Click(sender As Object, e As EventArgs) Handles btn_Update.Click
        Dim Update As Form = frm_Invoices_Update
        Update.ShowDialog()
        Update.BringToFront()
    End Sub

    Private Sub clearInputs()
        txt_Currency.Clear()    
        txt_VATCode.Clear()
        num_SellPrice.Value = 0
    End Sub

    Private Sub toggleButtons()
        If (btn_Add.Enabled) Then
            btn_Add.Enabled = False
            btn_Update.Enabled = False
            btn_Print.Enabled = False
            txt_SearchField.Enabled = False
            btn_Search.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Update.Enabled = True
            btn_Print.Enabled = True
            txt_SearchField.Enabled = True
            btn_Search.Enabled = True
        End If
    End Sub

End Class