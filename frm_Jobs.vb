﻿Public Class frm_Jobs
    Private session As Session

    Private Sub RefreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Job")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Job")
    End Sub

    Public Sub New(session As Session)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.session = session
    End Sub

    Private Sub frm_Jobs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplitContainer1.Panel1Collapsed = True

        DatabaseManager.RunSelectAllOnTable("tbl_Client")
        For Each client As DataRow In DatabaseManager.ds.Tables("tbl_Client").Rows
            cb_Client.Items.Add(client("ClientID") & ": " & client("FirstName") & " " & client("LastName"))
        Next

        Try
            cb_Client.SelectedIndex = 0
        Catch ex As Exception
            'If there's no clients, the selectedindex setter will throw an exception. Handle this with an error and close the form.
            MessageBox.Show("You must have at least one client in the database to create a job")
            Me.Close()
        End Try

        RefreshDataView()
    End Sub

    Private Sub cb_Client_SelectedValueChanged(sender As Object, e As EventArgs) Handles cb_Client.SelectedValueChanged
        PopulateQuotesListBox()
    End Sub

    Sub PopulateQuotesListBox()
        Try
            DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Quote WHERE ClientID='{0}' AND Reference NOT IN (SELECT QuoteReference FROM tbl_Job) AND DateAccepted IS NOT NULL AND DateDeclined IS NULL", cb_Client.SelectedItem.Remove(cb_Client.SelectedItem.IndexOf(":"))), "QuotesForClients")
            lb_Quote.Items.Clear()
            For Each quote As DataRow In DatabaseManager.ds.Tables("QuotesForClients").Rows
                lb_Quote.Items.Add(quote("Reference") & ": " & quote("QuoteDate") & " £" & quote("QuotedPrice"))
            Next
        Catch ex As NullReferenceException
            MessageBox.Show("No quotes currently in the system.")
            Me.Close()
        End Try
    End Sub

    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation
        If (lb_Quote.SelectedItem = "") Then
            MessageBox.Show("No quote selected")
            Return
        End If

        'Save new job
        DatabaseManager.RunSelectAllOnTable("tbl_Job")
        Dim newJob As DataRow = DatabaseManager.ds.Tables("tbl_Job").NewRow()

        newJob("JobID") = DatabaseManager.getNextID("tbl_Job")
        newJob("QuoteReference") = lb_Quote.SelectedItem.Remove(lb_Quote.SelectedItem.IndexOf(":"))
        newJob("DateStarted") = Date.Now.ToString()
        newJob("CheckedBy") = Me.session.StaffID

        DatabaseManager.Update(newJob, "tbl_Job")

        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
        RefreshDataView()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    'Bottom options
    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
        PopulateQuotesListBox()
    End Sub

    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Job", index)
        Next

        RefreshDataView()
    End Sub


    Private Sub clearInputs()
        lb_Quote.Items.Clear()
    End Sub

    Private Sub toggleButtons()
        If (btn_Add.Enabled = True) Then
            btn_Add.Enabled = False
            btn_Delete.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Delete.Enabled = True
        End If
    End Sub

End Class