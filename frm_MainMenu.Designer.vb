﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_MainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_MainMenu))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lbl_Name = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btn_MyAccount = New System.Windows.Forms.Button()
        Me.btn_Logout = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_BackupRestore = New System.Windows.Forms.Button()
        Me.btn_StaffManagement = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btn_Clients = New System.Windows.Forms.Button()
        Me.btn_Invoices = New System.Windows.Forms.Button()
        Me.btn_Quotes = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btn_Specifications = New System.Windows.Forms.Button()
        Me.btn_Jobs = New System.Windows.Forms.Button()
        Me.btn_Outwork = New System.Windows.Forms.Button()
        Me.btn_Artwork = New System.Windows.Forms.Button()
        Me.btn_Deliveries = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lbl_Name)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(683, 47)
        Me.Panel1.TabIndex = 1
        '
        'lbl_Name
        '
        Me.lbl_Name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Name.Location = New System.Drawing.Point(410, 22)
        Me.lbl_Name.Name = "lbl_Name"
        Me.lbl_Name.Size = New System.Drawing.Size(255, 20)
        Me.lbl_Name.TabIndex = 2
        Me.lbl_Name.Text = "<Name Of User>"
        Me.lbl_Name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(586, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Welcome Back,"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(185, 39)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'btn_MyAccount
        '
        Me.btn_MyAccount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_MyAccount.Image = CType(resources.GetObject("btn_MyAccount.Image"), System.Drawing.Image)
        Me.btn_MyAccount.Location = New System.Drawing.Point(7, 19)
        Me.btn_MyAccount.Name = "btn_MyAccount"
        Me.btn_MyAccount.Size = New System.Drawing.Size(111, 35)
        Me.btn_MyAccount.TabIndex = 5
        Me.btn_MyAccount.Text = "My Account"
        Me.btn_MyAccount.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_MyAccount.UseVisualStyleBackColor = True
        '
        'btn_Logout
        '
        Me.btn_Logout.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Logout.Image = CType(resources.GetObject("btn_Logout.Image"), System.Drawing.Image)
        Me.btn_Logout.Location = New System.Drawing.Point(6, 60)
        Me.btn_Logout.Name = "btn_Logout"
        Me.btn_Logout.Size = New System.Drawing.Size(111, 35)
        Me.btn_Logout.TabIndex = 4
        Me.btn_Logout.Text = "Logout"
        Me.btn_Logout.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Logout.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_Logout)
        Me.GroupBox1.Controls.Add(Me.btn_MyAccount)
        Me.GroupBox1.Location = New System.Drawing.Point(548, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(123, 103)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "My Menu"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_BackupRestore)
        Me.GroupBox2.Controls.Add(Me.btn_StaffManagement)
        Me.GroupBox2.Location = New System.Drawing.Point(374, 53)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(168, 103)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Director"
        '
        'btn_BackupRestore
        '
        Me.btn_BackupRestore.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_BackupRestore.Image = CType(resources.GetObject("btn_BackupRestore.Image"), System.Drawing.Image)
        Me.btn_BackupRestore.Location = New System.Drawing.Point(6, 60)
        Me.btn_BackupRestore.Name = "btn_BackupRestore"
        Me.btn_BackupRestore.Size = New System.Drawing.Size(156, 35)
        Me.btn_BackupRestore.TabIndex = 6
        Me.btn_BackupRestore.Text = "Backup/Restore"
        Me.btn_BackupRestore.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_BackupRestore.UseVisualStyleBackColor = True
        '
        'btn_StaffManagement
        '
        Me.btn_StaffManagement.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_StaffManagement.Image = CType(resources.GetObject("btn_StaffManagement.Image"), System.Drawing.Image)
        Me.btn_StaffManagement.Location = New System.Drawing.Point(6, 19)
        Me.btn_StaffManagement.Name = "btn_StaffManagement"
        Me.btn_StaffManagement.Size = New System.Drawing.Size(158, 35)
        Me.btn_StaffManagement.TabIndex = 5
        Me.btn_StaffManagement.Text = "Staff Management"
        Me.btn_StaffManagement.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_StaffManagement.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btn_Clients)
        Me.GroupBox3.Controls.Add(Me.btn_Invoices)
        Me.GroupBox3.Controls.Add(Me.btn_Quotes)
        Me.GroupBox3.Location = New System.Drawing.Point(237, 53)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(131, 148)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Finance"
        '
        'btn_Clients
        '
        Me.btn_Clients.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Clients.Image = CType(resources.GetObject("btn_Clients.Image"), System.Drawing.Image)
        Me.btn_Clients.Location = New System.Drawing.Point(6, 19)
        Me.btn_Clients.Name = "btn_Clients"
        Me.btn_Clients.Size = New System.Drawing.Size(119, 35)
        Me.btn_Clients.TabIndex = 7
        Me.btn_Clients.Text = "Clients"
        Me.btn_Clients.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Clients.UseVisualStyleBackColor = True
        '
        'btn_Invoices
        '
        Me.btn_Invoices.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Invoices.Image = CType(resources.GetObject("btn_Invoices.Image"), System.Drawing.Image)
        Me.btn_Invoices.Location = New System.Drawing.Point(6, 101)
        Me.btn_Invoices.Name = "btn_Invoices"
        Me.btn_Invoices.Size = New System.Drawing.Size(119, 35)
        Me.btn_Invoices.TabIndex = 6
        Me.btn_Invoices.Text = "Invoices"
        Me.btn_Invoices.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Invoices.UseVisualStyleBackColor = True
        '
        'btn_Quotes
        '
        Me.btn_Quotes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Quotes.Image = CType(resources.GetObject("btn_Quotes.Image"), System.Drawing.Image)
        Me.btn_Quotes.Location = New System.Drawing.Point(6, 60)
        Me.btn_Quotes.Name = "btn_Quotes"
        Me.btn_Quotes.Size = New System.Drawing.Size(119, 35)
        Me.btn_Quotes.TabIndex = 5
        Me.btn_Quotes.Text = "Quotes"
        Me.btn_Quotes.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Quotes.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btn_Specifications)
        Me.GroupBox4.Controls.Add(Me.btn_Jobs)
        Me.GroupBox4.Controls.Add(Me.btn_Outwork)
        Me.GroupBox4.Controls.Add(Me.btn_Artwork)
        Me.GroupBox4.Controls.Add(Me.btn_Deliveries)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 53)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(219, 148)
        Me.GroupBox4.TabIndex = 9
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Jobs"
        '
        'btn_Specifications
        '
        Me.btn_Specifications.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Specifications.Image = CType(resources.GetObject("btn_Specifications.Image"), System.Drawing.Image)
        Me.btn_Specifications.Location = New System.Drawing.Point(114, 19)
        Me.btn_Specifications.Name = "btn_Specifications"
        Me.btn_Specifications.Size = New System.Drawing.Size(99, 35)
        Me.btn_Specifications.TabIndex = 10
        Me.btn_Specifications.Text = "Specifications"
        Me.btn_Specifications.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Specifications.UseVisualStyleBackColor = True
        '
        'btn_Jobs
        '
        Me.btn_Jobs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Jobs.Image = CType(resources.GetObject("btn_Jobs.Image"), System.Drawing.Image)
        Me.btn_Jobs.Location = New System.Drawing.Point(6, 19)
        Me.btn_Jobs.Name = "btn_Jobs"
        Me.btn_Jobs.Size = New System.Drawing.Size(102, 35)
        Me.btn_Jobs.TabIndex = 9
        Me.btn_Jobs.Text = "Jobs"
        Me.btn_Jobs.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Jobs.UseVisualStyleBackColor = True
        '
        'btn_Outwork
        '
        Me.btn_Outwork.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Outwork.Image = CType(resources.GetObject("btn_Outwork.Image"), System.Drawing.Image)
        Me.btn_Outwork.Location = New System.Drawing.Point(6, 60)
        Me.btn_Outwork.Name = "btn_Outwork"
        Me.btn_Outwork.Size = New System.Drawing.Size(102, 35)
        Me.btn_Outwork.TabIndex = 8
        Me.btn_Outwork.Text = "Outwork"
        Me.btn_Outwork.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Outwork.UseVisualStyleBackColor = True
        '
        'btn_Artwork
        '
        Me.btn_Artwork.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Artwork.Image = CType(resources.GetObject("btn_Artwork.Image"), System.Drawing.Image)
        Me.btn_Artwork.Location = New System.Drawing.Point(114, 60)
        Me.btn_Artwork.Name = "btn_Artwork"
        Me.btn_Artwork.Size = New System.Drawing.Size(99, 35)
        Me.btn_Artwork.TabIndex = 6
        Me.btn_Artwork.Text = "Artwork"
        Me.btn_Artwork.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Artwork.UseVisualStyleBackColor = True
        '
        'btn_Deliveries
        '
        Me.btn_Deliveries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Deliveries.Image = CType(resources.GetObject("btn_Deliveries.Image"), System.Drawing.Image)
        Me.btn_Deliveries.Location = New System.Drawing.Point(6, 101)
        Me.btn_Deliveries.Name = "btn_Deliveries"
        Me.btn_Deliveries.Size = New System.Drawing.Size(105, 35)
        Me.btn_Deliveries.TabIndex = 5
        Me.btn_Deliveries.Text = "Deliveries"
        Me.btn_Deliveries.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_Deliveries.UseVisualStyleBackColor = True
        '
        'frm_MainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 212)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frm_MainMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Main Menu"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btn_Logout As System.Windows.Forms.Button
    Friend WithEvents lbl_Name As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_MyAccount As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_StaffManagement As System.Windows.Forms.Button
    Friend WithEvents btn_BackupRestore As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_Invoices As System.Windows.Forms.Button
    Friend WithEvents btn_Quotes As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_Artwork As System.Windows.Forms.Button
    Friend WithEvents btn_Deliveries As System.Windows.Forms.Button
    Friend WithEvents btn_Outwork As System.Windows.Forms.Button
    Friend WithEvents btn_Jobs As System.Windows.Forms.Button
    Friend WithEvents btn_Clients As System.Windows.Forms.Button
    Friend WithEvents btn_Specifications As System.Windows.Forms.Button
End Class
