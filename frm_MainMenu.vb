﻿Public Class frm_MainMenu
    Private ReadOnly session As Session
    Private loggingOut As Boolean = False

    'Menu options
    Sub New(ByVal s As Session)
        InitializeComponent()
        session = s

        'Change name to user name.
        lbl_Name.Text = session.Name

        'Lock buttons if employee.
        If (session.GroupID <> "1") Then
            btn_StaffManagement.Enabled = False
            btn_BackupRestore.Enabled = False
        End If
    End Sub

    Private Sub btn_StaffManagement_Click(sender As Object, e As EventArgs) Handles btn_StaffManagement.Click
        'Staff Management
        If (session.GroupID = "1") Then
            Dim StaffMan As Form = frm_StaffManagement
            StaffMan.Show()
            StaffMan.BringToFront()
        Else
            MessageBox.Show("Access Denied")
        End If
    End Sub

    Private Sub btn_BackupRestore_Click(sender As Object, e As EventArgs) Handles btn_BackupRestore.Click
        'Backup/Restore
        If (session.GroupID = "1") Then
            Dim BackupRestore As Form = frm_BackupRestore
            BackupRestore.Show()
            BackupRestore.BringToFront()
        Else
            MessageBox.Show("Access Denied")
        End If
    End Sub

    Private Sub btn_MyAccount_Click(sender As Object, e As EventArgs) Handles btn_MyAccount.Click
        'My Account
        Dim MyAccount As Form = New frm_MyAccount(session)
        MyAccount.Show()
        MyAccount.BringToFront()
    End Sub

    Private Sub btn_Clients_Click(sender As Object, e As EventArgs) Handles btn_Clients.Click
        Dim Clients As Form = frm_Clients
        Clients.Show()
        Clients.BringToFront()
    End Sub

    Private Sub btn_Specifications_Click(sender As Object, e As EventArgs) Handles btn_Specifications.Click
        'Specifications
        Dim Specifications As Form = frm_Specifications
        Specifications.Show()
        Specifications.BringToFront()
    End Sub

    Private Sub btn_Invoices_Click(sender As Object, e As EventArgs) Handles btn_Invoices.Click
        'Invoices
        Dim Invoices As Form = New frm_Invoices(session)
        Invoices.Show()
        Invoices.BringToFront()
    End Sub

    Private Sub btn_Outwork_Click(sender As Object, e As EventArgs) Handles btn_Outwork.Click
        'Outwork
        Dim Outwork As Form = frm_Outwork
        Outwork.Show()
        Outwork.BringToFront()
    End Sub

    Private Sub btn_Artwork_Click(sender As Object, e As EventArgs) Handles btn_Artwork.Click
        'Artwork
        Dim Artwork As Form = frm_Artwork
        Artwork.Show()
        Artwork.BringToFront()
    End Sub

    Private Sub btn_Quotes_Click(sender As Object, e As EventArgs) Handles btn_Quotes.Click
        'Quotes
        Dim Quotes As Form = frm_Quotes
        Quotes.Show()
        Quotes.BringToFront()
    End Sub

    Private Sub btn_Deliveries_Click(sender As Object, e As EventArgs) Handles btn_Deliveries.Click
        'Deliveries
        Dim Deliveries As Form = frm_Deliveries
        Deliveries.Show()
        Deliveries.BringToFront()
    End Sub

    Private Sub btn_Jobs_Click(sender As Object, e As EventArgs) Handles btn_Jobs.Click
        'Jobs
        Dim Jobs As Form = New frm_Jobs(Me.session)
        Jobs.Show()
        Jobs.BringToFront()
    End Sub

    Private Sub btn_Logout_Click(sender As Object, e As EventArgs) Handles btn_Logout.Click
        logout()
    End Sub

    'Form events
    Private Sub frm_MainMenu_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If (Not loggingOut) Then
            Application.Exit()
        End If
    End Sub

    'Misc
    Sub logout()
        loggingOut = True
        frm_Login.Visible = True
        Me.Close()
    End Sub

    Private Sub frm_MainMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class