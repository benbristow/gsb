﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_MyAccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_MyAccount))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ll_Email = New System.Windows.Forms.LinkLabel()
        Me.lbl_Name = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_ChangePassword = New System.Windows.Forms.Button()
        Me.txt_PasswordNewConfirm = New System.Windows.Forms.TextBox()
        Me.txt_PasswordNew = New System.Windows.Forms.TextBox()
        Me.txt_PasswordCurrent = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ll_Email)
        Me.GroupBox1.Controls.Add(Me.lbl_Name)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(296, 67)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "My Information"
        '
        'll_Email
        '
        Me.ll_Email.AutoSize = True
        Me.ll_Email.Location = New System.Drawing.Point(53, 39)
        Me.ll_Email.Name = "ll_Email"
        Me.ll_Email.Size = New System.Drawing.Size(119, 13)
        Me.ll_Email.TabIndex = 4
        Me.ll_Email.TabStop = True
        Me.ll_Email.Text = "example@example.com"
        '
        'lbl_Name
        '
        Me.lbl_Name.AutoSize = True
        Me.lbl_Name.Location = New System.Drawing.Point(55, 16)
        Me.lbl_Name.Name = "lbl_Name"
        Me.lbl_Name.Size = New System.Drawing.Size(122, 13)
        Me.lbl_Name.TabIndex = 2
        Me.lbl_Name.Text = "<Employee Name Here>"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Email:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_ChangePassword)
        Me.GroupBox2.Controls.Add(Me.txt_PasswordNewConfirm)
        Me.GroupBox2.Controls.Add(Me.txt_PasswordNew)
        Me.GroupBox2.Controls.Add(Me.txt_PasswordCurrent)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 85)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(296, 205)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Change Password"
        '
        'btn_ChangePassword
        '
        Me.btn_ChangePassword.Image = CType(resources.GetObject("btn_ChangePassword.Image"), System.Drawing.Image)
        Me.btn_ChangePassword.Location = New System.Drawing.Point(169, 168)
        Me.btn_ChangePassword.Name = "btn_ChangePassword"
        Me.btn_ChangePassword.Size = New System.Drawing.Size(121, 23)
        Me.btn_ChangePassword.TabIndex = 6
        Me.btn_ChangePassword.Text = "Change Password"
        Me.btn_ChangePassword.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btn_ChangePassword.UseVisualStyleBackColor = True
        '
        'txt_PasswordNewConfirm
        '
        Me.txt_PasswordNewConfirm.Location = New System.Drawing.Point(6, 142)
        Me.txt_PasswordNewConfirm.Name = "txt_PasswordNewConfirm"
        Me.txt_PasswordNewConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txt_PasswordNewConfirm.Size = New System.Drawing.Size(284, 20)
        Me.txt_PasswordNewConfirm.TabIndex = 5
        '
        'txt_PasswordNew
        '
        Me.txt_PasswordNew.Location = New System.Drawing.Point(9, 93)
        Me.txt_PasswordNew.Name = "txt_PasswordNew"
        Me.txt_PasswordNew.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txt_PasswordNew.Size = New System.Drawing.Size(281, 20)
        Me.txt_PasswordNew.TabIndex = 4
        '
        'txt_PasswordCurrent
        '
        Me.txt_PasswordCurrent.Location = New System.Drawing.Point(9, 45)
        Me.txt_PasswordCurrent.Name = "txt_PasswordCurrent"
        Me.txt_PasswordCurrent.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txt_PasswordCurrent.Size = New System.Drawing.Size(281, 20)
        Me.txt_PasswordCurrent.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 126)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(148, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "New Password (Confirm):"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 77)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(172, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "New Password (Min 7 chars):"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Current Password:"
        '
        'frm_MyAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(314, 302)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_MyAccount"
        Me.Text = "My Account"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_PasswordNewConfirm As System.Windows.Forms.TextBox
    Friend WithEvents txt_PasswordNew As System.Windows.Forms.TextBox
    Friend WithEvents txt_PasswordCurrent As System.Windows.Forms.TextBox
    Friend WithEvents btn_ChangePassword As System.Windows.Forms.Button
    Friend WithEvents lbl_Name As System.Windows.Forms.Label
    Friend WithEvents ll_Email As System.Windows.Forms.LinkLabel
End Class
