﻿Public Class frm_MyAccount
    Private session As Session

    Sub New(ByVal sess As Session)
        InitializeComponent()
        lbl_Name.Text = sess.Name
        ll_Email.Text = sess.Email
        session = sess
    End Sub

    Private Sub ll_Email_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles ll_Email.LinkClicked
        openUrlInBrowser("mailto:" & ll_Email.Text)
    End Sub

    Private Sub btn_ChangePassword_Click(sender As Object, e As EventArgs) Handles btn_ChangePassword.Click
        DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Staff WHERE StaffID='{0}';", session.StaffID), "tbl_Staff")
        Dim staff As DataTable = DatabaseManager.ds.Tables("tbl_Staff")
        Dim member As DataRow = staff.Rows(0)

        Dim currentpasshash As String = member("PasswordHash")

        If (Not VerifyPassword(currentpasshash, txt_PasswordCurrent.Text)) Then
            MessageBox.Show("Invalid Current Password")
            Return
        End If

        If (txt_PasswordNew.Text <> txt_PasswordNewConfirm.Text) Then
            MessageBox.Show("Passwords do not match")
            Return
        End If

        If (txt_PasswordNew.Text.Count < 7) Then
            MessageBox.Show("Passwords must be 7 characters long or greater")
            Return
        End If


        DatabaseManager.RunSQL(String.Format("UPDATE tbl_Staff SET PasswordHash='{0}' WHERE StaffID='{1}';", EncryptPassword(txt_PasswordNew.Text), session.StaffID), "tbl_Staff")
        MessageBox.Show("Password change successful!")
    End Sub

    Private Sub frm_MyAccount_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class