﻿Public Class frm_Outwork

    Private Sub RefreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Outwork")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Outwork")
        DataGridView1.ClearSelection()
    End Sub

    Private Sub frm_Artwork_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplitContainer1.Panel1Collapsed = True

        'Add items to comboboxes
        DatabaseManager.RunSelectAllOnTable("tbl_Specifications")
        For Each spec As DataRow In DatabaseManager.ds.Tables("tbl_Specifications").Rows
            cb_Specification.Items.Add(spec("SpecID") & ": " & spec("Title"))
        Next

        DatabaseManager.RunSelectAllOnTable("tbl_OutworkSupplier")
        For Each supplier As DataRow In DatabaseManager.ds.Tables("tbl_OutworkSupplier").Rows
            cb_Supplier.Items.Add(supplier("OSupplierID") & ": " & supplier("CompanyName"))
        Next

        'Refresh data view
        RefreshDataView()
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
    End Sub

    'Add panel
    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Check date is not in the past
        If (date_ExpectedDate.Value < Date.Today) Then
            MessageBox.Show("Date can not be in the past!")
            'Reset date to today's date.
            date_ExpectedDate.Value = Date.Today
            Return
        End If

        'Presence validation
        If (cb_Specification.SelectedItem = "") Then
            MessageBox.Show("No specification selected")
            Return
        End If

        If (cb_Supplier.SelectedItem = "") Then
            MessageBox.Show("No supplier selected")
        End If

        'Price validation
        Try
            Single.Parse(txt_Price.Text)
        Catch ex As Exception
            MessageBox.Show("Price is not a valid number")
        End Try

        'And now save ^_^
        DatabaseManager.RunSelectAllOnTable("tbl_Outwork")
        Dim newOutwork As DataRow = DatabaseManager.ds.Tables("tbl_Outwork").NewRow()

        newOutwork("OutworkID") = DatabaseManager.getNextID("tbl_Outwork")
        newOutwork("SpecID") = cb_Specification.SelectedItem.Remove(cb_Specification.SelectedItem.IndexOf(":"))
        newOutwork("SupplierID") = cb_Supplier.SelectedItem.Remove(cb_Supplier.SelectedItem.IndexOf(":"))
        newOutwork("ExpectedDate") = date_ExpectedDate.Value.ToString()
        newOutwork("Price") = txt_Price.Text

        DatabaseManager.Update(newOutwork, "tbl_Outwork")

        'Update GUI elements
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()

        'Refresh data view
        RefreshDataView()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    'Bottom options
    Private Sub btn_Schedule_Click(sender As Object, e As EventArgs) Handles btn_Schedule.Click
        'Show schedule panel
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub

    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Outwork", index)
        Next

        'Refresh data view
        RefreshDataView()
    End Sub

    Private Sub btn_MarkAsFinished_Click(sender As Object, e As EventArgs) Handles btn_MarkAsFinished.Click
        'Mark as Finished
        If (DataGridView1.SelectedRows.Count = 0) Then
            MessageBox.Show("No rows selected")
            Return
        End If

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            Dim OutworkID As String = item.Cells("OutworkID").Value
            DatabaseManager.RunSQL(String.Format("UPDATE tbl_Outwork SET DateFinished='{0}' WHERE OutworkID='{1}';", Date.Now.ToString(), OutworkID), "tbl_Outwork")
        Next

        RefreshDataView()
    End Sub

    'Procedures
    Private Sub clearInputs()
        cb_Specification.SelectedItem = ""
        cb_Supplier.SelectedItem = ""
        txt_Price.Text = ""
    End Sub

    Private Sub toggleButtons()
        If (btn_Schedule.Enabled = True) Then
            btn_Schedule.Enabled = False
            btn_Delete.Enabled = False
            btn_MarkAsFinished.Enabled = False
        Else
            btn_Schedule.Enabled = True
            btn_Delete.Enabled = True
            btn_MarkAsFinished.Enabled = True
        End If
    End Sub
End Class