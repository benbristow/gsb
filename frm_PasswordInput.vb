﻿Imports System.Windows.Forms

Public Class frm_PasswordInput
    Private message As String
    Private title As String
    Private password As String

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.password = TextBox1.Text
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frm_PasswordInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label1.Text = Me.title
        Me.Text = Me.title
        Label2.Text = Me.message

        TextBox1.Select()
    End Sub

    Public Sub New(ByVal message As String, ByVal title As String)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.        
        Me.title = title
        Me.message = message
    End Sub

    Public Function getPassword()
        Return Me.password
    End Function
End Class
