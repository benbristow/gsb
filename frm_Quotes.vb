﻿Public Class frm_Quotes

    Private Sub RefreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Quote")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Quote")
        DataGridView1.ClearSelection()
        ColourDataGridView()
    End Sub

    Private Sub ColourDataGridView()
        'Colour the data grid view depending on the status of the quote

        'For each row in the datagridview
        For Each row As DataGridViewRow In DataGridView1.Rows        
            If (row.Cells("DateAccepted").Value.ToString().Length > 0) Then
                'If accepted, colour lime
                row.DefaultCellStyle.BackColor = Color.Lime
            ElseIf (row.Cells("DateDeclined").Value.ToString().Length > 0) Then
                'If declined, colour orange/red.
                row.DefaultCellStyle.BackColor = Color.OrangeRed
            Else
                'If neither accepted nor declined, colour sandy brown.
                row.DefaultCellStyle.BackColor = Color.SandyBrown
            End If
        Next
    End Sub

    Private Sub DataGridView1_Sorted(sender As Object, e As EventArgs) Handles DataGridView1.Sorted
        'For some reason if you sort the datagridview, the colours of the rows reset to white.
        'This will re-instate the colours.
        ColourDataGridView()
    End Sub

    Private Sub frm_Quotes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplitContainer1.Panel1Collapsed = True

        'Update combo boxes.
        DatabaseManager.RunSelectAllOnTable("tbl_Client")
        For Each client As DataRow In DatabaseManager.ds.Tables("tbl_Client").Rows
            cb_Client.Items.Add(client("ClientID") & ": " & client("FirstName") & " " & client("LastName"))
        Next

        RefreshDataView()
    End Sub

    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation

        'Check reference number has been inputted
        If (txt_ReferenceNumber.Text.Length = 0) Then
            MessageBox.Show("No quote reference number added")
            Return
        End If

        'Check reference number is numeric.
        If (Not IsNumeric(txt_ReferenceNumber.Text)) Then
            MessageBox.Show("Reference number must not contain non-numeric characters.")
            Return
        End If

        'Check reference number is unique
        DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Quote WHERE Reference='{0}'", txt_ReferenceNumber.Text), "tbl_Quote")
        If (DatabaseManager.ds.Tables("tbl_Quote").Rows.Count > 0) Then
            MessageBox.Show("Reference number must be unique")
            Return
        End If

        'Check client has been selected
        If (cb_Client.SelectedItem = "") Then
            MessageBox.Show("No client selected")
            Return
        End If

        'Save new quote
        DatabaseManager.RunSelectAllOnTable("tbl_Quote")
        Dim newQuote As DataRow = DatabaseManager.ds.Tables("tbl_Quote").NewRow()

        newQuote("Reference") = txt_ReferenceNumber.Text
        newQuote("QuoteDate") = Date.Now.ToString
        newQuote("QuotedPrice") = num_QuotedPrice.Value.ToString
        newQuote("ClientID") = cb_Client.SelectedItem.Remove(cb_Client.SelectedItem.IndexOf(":"))
        newQuote("Notes") = rtb_Notes.Text

        DatabaseManager.Update(newQuote, "tbl_Quote")

        'Update GUI
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
        RefreshDataView()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub

    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Quote", index)
        Next

        RefreshDataView()
    End Sub

    Private Sub btn_MarkAsAccepted_Click(sender As Object, e As EventArgs) Handles btn_MarkAsAccepted.Click
        'Approval
        If (DataGridView1.SelectedRows.Count = 0) Then
            MessageBox.Show("No rows selected")
            Return
        End If

        Dim indicies As New List(Of String)
        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.RunSQL(String.Format("UPDATE tbl_Quote SET DateAccepted='{0}' WHERE Reference='{1}';", Date.Now.ToString(), index), "tbl_Quote")
        Next

        RefreshDataView()
    End Sub

    Private Sub btn_MarkAsDeclined_Click(sender As Object, e As EventArgs) Handles btn_MarkAsDeclined.Click
        'Decline
        If (DataGridView1.SelectedRows.Count = 0) Then
            MessageBox.Show("No rows selected")
            Return
        End If

        Dim indicies As New List(Of String)
        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.RunSQL(String.Format("UPDATE tbl_Quote SET DateDeclined='{0}' WHERE Reference='{1}';", Date.Now.ToString(), index), "tbl_Quote")
        Next

        RefreshDataView()
    End Sub

    Private Sub clearInputs()
        txt_ReferenceNumber.Text = ""
        num_QuotedPrice.Value = 0
        rtb_Notes.Text = ""
    End Sub

    Private Sub toggleButtons()
        If (btn_Add.Enabled = True) Then
            btn_Add.Enabled = False
            btn_Delete.Enabled = False
            btn_MarkAsAccepted.Enabled = False
            btn_MarkAsDeclined.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Delete.Enabled = True
            btn_MarkAsAccepted.Enabled = True
            btn_MarkAsDeclined.Enabled = True
        End If
    End Sub



End Class