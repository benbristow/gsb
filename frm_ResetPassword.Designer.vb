﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ResetPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_ResetCode = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_Finish = New System.Windows.Forms.Button()
        Me.txt_Password = New System.Windows.Forms.TextBox()
        Me.txt_PasswordConfirm = New System.Windows.Forms.TextBox()
        Me.btn_Previous = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txt_ResetCode
        '
        Me.txt_ResetCode.Location = New System.Drawing.Point(98, 34)
        Me.txt_ResetCode.Name = "txt_ResetCode"
        Me.txt_ResetCode.Size = New System.Drawing.Size(257, 20)
        Me.txt_ResetCode.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(343, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "You should receieve a reset code via email within the next few minutes."
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Reset Code:"
        '
        'btn_Finish
        '
        Me.btn_Finish.Location = New System.Drawing.Point(292, 112)
        Me.btn_Finish.Name = "btn_Finish"
        Me.btn_Finish.Size = New System.Drawing.Size(63, 23)
        Me.btn_Finish.TabIndex = 3
        Me.btn_Finish.Text = "Finish"
        Me.btn_Finish.UseVisualStyleBackColor = True
        '
        'txt_Password
        '
        Me.txt_Password.Location = New System.Drawing.Point(189, 60)
        Me.txt_Password.Name = "txt_Password"
        Me.txt_Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txt_Password.Size = New System.Drawing.Size(166, 20)
        Me.txt_Password.TabIndex = 1
        '
        'txt_PasswordConfirm
        '
        Me.txt_PasswordConfirm.Location = New System.Drawing.Point(189, 86)
        Me.txt_PasswordConfirm.Name = "txt_PasswordConfirm"
        Me.txt_PasswordConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txt_PasswordConfirm.Size = New System.Drawing.Size(166, 20)
        Me.txt_PasswordConfirm.TabIndex = 2
        '
        'btn_Previous
        '
        Me.btn_Previous.Location = New System.Drawing.Point(12, 112)
        Me.btn_Previous.Name = "btn_Previous"
        Me.btn_Previous.Size = New System.Drawing.Size(80, 23)
        Me.btn_Previous.TabIndex = 4
        Me.btn_Previous.Text = "← Previous"
        Me.btn_Previous.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 63)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(171, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "New Password (min 7 chars):"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(72, 89)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(111, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Confirm Password:"
        '
        'frm_ResetPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 144)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btn_Previous)
        Me.Controls.Add(Me.txt_PasswordConfirm)
        Me.Controls.Add(Me.txt_Password)
        Me.Controls.Add(Me.btn_Finish)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_ResetCode)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_ResetPassword"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reset Password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_ResetCode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn_Finish As System.Windows.Forms.Button
    Friend WithEvents txt_Password As System.Windows.Forms.TextBox
    Friend WithEvents txt_PasswordConfirm As System.Windows.Forms.TextBox
    Friend WithEvents btn_Previous As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
