﻿Public Class frm_ResetPassword

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_Finish.Click
        'Finish
        Try
            ResetPassword()
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btn_Previous.Click
        'Previous
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ResetPassword()
        'Password validations
        If (txt_Password.Text <> txt_PasswordConfirm.Text) Then
            Throw New Exception("Password & Password Confirmation do not match")
        End If

        If (txt_Password.Text.Count < 7) Then
            Throw New Exception("Password must be at least 7 characters long")
        End If

        'Check reset code exists
        DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Staff WHERE ResetCode='{0}'", txt_ResetCode.Text), "tbl_Staff")

        If (ds.Tables("tbl_Staff").Rows.Count = 0) Then
            Throw New Exception("Reset Code does not match any user")
        End If

        'Hash Password
        Dim PasswordHash As String = EncryptPassword(txt_Password.Text)

        'Update!
        DatabaseManager.RunSQL(String.Format("UPDATE tbl_Staff SET PasswordHash='{0}' WHERE ResetCode='{1}';", PasswordHash, txt_ResetCode.Text), "tbl_Staff")
        MessageBox.Show("Password successfully reset!")
    End Sub
End Class