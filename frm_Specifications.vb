﻿Public Class frm_Specifications

    Private Sub frm_Specifications_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplitContainer1.Panel1Collapsed = True

        'Add 'ALL' job filter
        cb_JobFilter.Items.Add("ALL")

        'Update comboboxes with Jobs.
        DatabaseManager.RunSelectAllOnTable("tbl_Job")
        For Each job As DataRow In DatabaseManager.ds.Tables("tbl_Job").Rows
            cb_Job.Items.Add(job("JobID") & ": " & job("QuoteReference"))
            cb_JobFilter.Items.Add(job("JobID") & ": " & job("QuoteReference"))
        Next

        'Select 'ALL' on job filter
        cb_JobFilter.SelectedIndex = 0

        RefreshDataView()
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs)
    End Sub

    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation
        If (txt_Title.Text.Length = 0) Then
            MessageBox.Show("Not all required inputs are complete.", "Missing Inputs", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        'Save new specification
        DatabaseManager.RunSelectAllOnTable("tbl_Specifications")
        Dim newSpec As DataRow = DatabaseManager.ds.Tables("tbl_Specifications").NewRow()

        newSpec("SpecID") = DatabaseManager.getNextID("tbl_Specifications")
        newSpec("Title") = txt_Title.Text
        newSpec("JobID") = cb_Job.SelectedItem.Remove(cb_Job.SelectedItem.IndexOf(":"))
        newSpec("Comments") = rtb_Comments.Text

        DatabaseManager.Update(newSpec, "tbl_Specifications")

        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
        RefreshDataView()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    'Bottom options
    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub

    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            indicies.Add(item.Cells(0).Value)
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Specifications", index)
        Next

        RefreshDataView()
    End Sub

    Private Sub cb_JobFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_JobFilter.SelectedIndexChanged
        RefreshDataView()
    End Sub

    Private Sub clearInputs()
        txt_Title.Text = ""    
        rtb_Comments.Clear()
    End Sub

    Private Sub toggleButtons()
        If (btn_Add.Enabled = True) Then
            btn_Add.Enabled = False
            btn_Delete.Enabled = False
            cb_JobFilter.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Delete.Enabled = True
            cb_JobFilter.Enabled = True
        End If
    End Sub


    Private Sub RefreshDataView()
        'Refresh data view

        'If Job filter is ALL, show all specifications, else show the selected specifications for selected Job
        If (cb_JobFilter.SelectedItem = "ALL") Then
            DatabaseManager.RunSelectAllOnTable("tbl_Specifications")
            DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Specifications")
        Else
            DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Specifications WHERE JobID='{0}'", cb_JobFilter.SelectedItem.Remove(cb_JobFilter.SelectedItem.IndexOf(":"))), "tbl_SpecificationsFiltered")
            DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_SpecificationsFiltered")
        End If

    End Sub

End Class