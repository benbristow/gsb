﻿Public Class frm_StaffManagement

    Private Sub frm_StaffManagement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Collapse Panel
        SplitContainer1.Panel1Collapsed = True

        'Get possible groups
        DatabaseManager.RunSelectAllOnTable("tbl_Groups")

        For Each group As DataRow In ds.Tables("tbl_Groups").Rows
            cb_Group.Items.Add(group("Title"))        
        Next

        cb_Group.SelectedIndex = 1

        'Refresh data view
        RefreshDataView()
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
    End Sub

    Private Sub btn_Save_Click(sender As Object, e As EventArgs) Handles btn_Save.Click
        'Validation

        'Mandatory fields
        If (txt_FirstName.Text.Length = 0 Or txt_Surname.Text.Length = 0 Or txt_Email.Text.Length = 0) Then
            MessageBox.Show("Not all required inputs are complete.", "Missing Inputs", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        'Check email unique
        DatabaseManager.RunSQL(String.Format("SELECT * FROM tbl_Staff WHERE Email='{0}';", txt_Email.Text), "tbl_Staff")
        If (DatabaseManager.ds.Tables("tbl_Staff").Rows.Count > 0) Then
            MessageBox.Show("Email address must be unique")
            Return
        End If

        'Email validation
        Dim r As New RegexUtilities()
        If Not r.IsValidEmail(txt_Email.Text) Then
            MessageBox.Show("Invalid Email Address Format.", "Missing Inputs", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        'New password
        Dim password As String = ""
        Dim passwordInput As New frm_PasswordInput("Please enter a new password for this user", "New User Password")

        If (Not passwordInput.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Return
        End If

        If (CType(passwordInput.getPassword(), String).Length < 7) Then
            MessageBox.Show("Password must be longer than 7 characters long")
            Return
        End If

        password = passwordInput.getPassword()
        passwordInput.Dispose()

        'Save new user
        DatabaseManager.RunSelectAllOnTable("tbl_Staff")
        Dim newMember As DataRow = DatabaseManager.ds.Tables("tbl_Staff").NewRow()

        newMember("StaffID") = DatabaseManager.getNextID("tbl_Staff")
        newMember("FirstName") = txt_FirstName.Text
        newMember("LastName") = txt_Surname.Text
        newMember("GroupID") = (cb_Group.SelectedIndex + 1).ToString()
        newMember("PasswordHash") = EncryptPassword(password)
        newMember("Email") = txt_Email.Text

        DatabaseManager.Update(newMember, "tbl_Staff")

        'Refresh display
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    Private Sub btn_Cancel_Click(sender As Object, e As EventArgs) Handles btn_Cancel.Click
        'Cancel editing/adding user
        toggleButtons()
        SplitContainer1.Panel1Collapsed = True
        clearInputs()
    End Sub

    Private Sub btn_Add_Click(sender As Object, e As EventArgs) Handles btn_Add.Click
        'Show add panel
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub


    Private Sub btn_Edit_Click(sender As Object, e As EventArgs) Handles btn_Edit.Click
        'Show edit panel
        clearInputs()
        toggleButtons()
        SplitContainer1.Panel1Collapsed = False
    End Sub

    Private Sub btn_Delete_Click(sender As Object, e As EventArgs) Handles btn_Delete.Click
        'Delete selected rows
        Dim indicies As New List(Of String)

        For Each item As DataGridViewRow In DataGridView1.SelectedRows
            If (item.Cells(0).Value <> 1) Then
                indicies.Add(item.Cells(0).Value)
            End If
        Next

        For Each index As String In indicies
            DatabaseManager.deleteRow("tbl_Staff", index)
        Next

        RefreshDataView()
    End Sub

    Private Sub clearInputs()
        'Clear all inputs
        txt_FirstName.Clear()
        txt_Surname.Clear()
        txt_Email.Clear()
    End Sub

    Private Sub toggleButtons()
        'Toggle buttons on/off
        If (btn_Add.Enabled = True) Then
            btn_Add.Enabled = False
            btn_Edit.Enabled = False
            btn_Delete.Enabled = False
        Else
            btn_Add.Enabled = True
            btn_Edit.Enabled = True
            btn_Delete.Enabled = True
        End If
    End Sub

    Private Sub RefreshDataView()
        'Refresh data view
        DatabaseManager.RunSelectAllOnTable("tbl_Staff")
        DataGridView1.DataSource = DatabaseManager.ds.Tables("tbl_Staff")
        DataGridView1.ClearSelection()
    End Sub

End Class