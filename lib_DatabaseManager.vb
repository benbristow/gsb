﻿Module DatabaseManager
    'Set the Database Provider/Data Source string to access the Access DB.
    Dim dbProvider As String = "PROVIDER=Microsoft.Jet.OLEDB.4.0;"
    Dim dbSource As String = "Data Source=GSB.mdb"

    'Required to access the database. Dataset + OleDbConnection
    Dim con As New OleDb.OleDbConnection
    Public ds As New DataSet

    Sub New()
        'Connect to database with connection String
        con.ConnectionString = dbProvider & dbSource
        con.Open()
    End Sub


    Sub RunSQL(ByVal sql As String, ByVal tablename As String)
        'Create data adapter
        Dim da As New OleDb.OleDbDataAdapter(sql, con)

        'Remove table we're inputting into from the dataset.
        Try
            ds.Tables(tablename).Clear()
        Catch ex As NullReferenceException
            'If it doesn't exist, we don't need to do anything
        End Try

        'Fill the dataset with the data response from the SQL query.
        da.Fill(ds, tablename)
    End Sub

    Sub RunSelectAllOnTable(tablename)
        'Create a SQL 'SELECT * FROM tablename' query
        Dim sql As String = "SELECT * FROM " & tablename & ";"

        'Execute using the Data Adapter
        Dim da As New OleDb.OleDbDataAdapter(sql, con)

        'Remove table we're inputting into from the dataset.
        Try
            ds.Tables(tablename).Clear()
        Catch ex As NullReferenceException
            'If it doesn't exist, we don't need to do anything
        End Try

        'Fill the dataset with the data response from the SQL query.
        da.Fill(ds, tablename)
    End Sub

    Private Function getDataAdapterForTable(tablename)
        'Create a SQL 'SELECT * FROM tablename' query
        Dim sql As String = "SELECT * FROM " & tablename

        'Create new Data Adapter and run query
        Dim da As New OleDb.OleDbDataAdapter(sql, con)

        'Return data adapter
        Return da
    End Function

    Sub Update(datarow, tablename)
        'Get data adapter
        Dim da As OleDb.OleDbDataAdapter = getDataAdapterForTable(tablename)

        'Add data row to data set
        ds.Tables(tablename).Rows.Add(datarow)

        'Generate a command builder and get update command
        Dim cmdbuilder As New OleDb.OleDbCommandBuilder(da)
        cmdbuilder.GetUpdateCommand()

        'And then run the update command
        da.Update(ds, tablename)
    End Sub

    Sub deleteRow(tablename, index)
        'Run a SELECT * SQL query to get all rows from the table
        RunSelectAllOnTable(tablename)

        'Find the name of the first column. The first column is always the primary key ID.
        Dim idColumn As String = ds.Tables(tablename).Columns(0).ColumnName

        'Generate an SQL DELETE FROM WHERE query.
        Dim sql As String = String.Format("DELETE FROM {0} WHERE {1}=""{2}""", tablename, idColumn, index.ToString())

        'Execute SQL.
        RunSQL(sql, tablename)
    End Sub

    Function getNextID(tablename)
        'This function gets the next available ID for a table.
        'AKA rudimental autonumber replacement.

        'This assumes that the dataset already has the table loaded.

        Try
            'Find how many rows there are in the table.
            Dim count As Integer = ds.Tables(tablename).Rows.Count

            'Select the last row.
            Dim row As DataRow = ds.Tables(tablename).Rows(count - 1)

            'Find the ID of the last row.
            Dim lastID As Integer = CInt(row(0))

            'The next ID will just be Plus 1.
            Return lastID + 1
        Catch
            'If the table was empty, we default to the next ID being 1.
            Return 1
        End Try
    End Function

    Sub OpenConnection()
        'Open the connection to the Database 
        con.Open()
    End Sub

    Sub CloseConnection()
        'Close connection to the database.
        con.Close()
    End Sub
End Module

