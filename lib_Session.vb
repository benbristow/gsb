﻿'The session class is used to store the details of the currently logged-in user.
Public Class Session
    Public StaffID As String
    Public Name As String
    Public GroupID As String
    Public Email As String

    Sub New(ByVal row As DataRow)
        'Constructor. Takes an argument of the datarow of the user from the DB.
        Me.StaffID = row("StaffID")
        Me.Name = row("FirstName") & " " & row("LastName")
        Me.GroupID = row("GroupID")
        Me.Email = row("Email")
    End Sub
End Class
