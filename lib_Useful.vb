﻿'Dependencies required from .NET
Imports System.Diagnostics
Imports System.Globalization
Imports System.Text.RegularExpressions

'This is an open-source library that is used for encryption.
Imports BCrypt.Net.BCrypt

'Global module. Used for storing useful procedures/functions that are used throughout.
Module Useful
    'Opens a web URL in the computer's default browser (IE/Firefox/Chrome etc.)
    Sub openUrlInBrowser(ByVal url As String)
        'Create new process object
        Dim p As Process = New Process()

        'Create new start info object. This is used to tell the process what to do.
        Dim psi As ProcessStartInfo = New ProcessStartInfo()

        'Set the process file name to be the URL. Windows will read this and default to opening the link in a browser.
        psi.FileName = url

        'Apply the start info to the process.
        p.StartInfo = psi

        'Start the process/Open the browser.
        p.Start()
    End Sub

    Function EncryptPassword(ByVal password As String)
        'Uses the BCrypt Library. Takes a password as a string, generates a Salt and hashes it.
        'The greater the value, e.g. in this instance we're using 13, the more 'secure' the encryption will be, but the more processor time it will take to encrypt.
        '13 is a happy medium between security + performance.
        GenerateSalt(13)
        Return HashPassword(password, 13)
    End Function

    Function VerifyPassword(ByVal hash As String, ByVal attempt As String)
        Try
            'Uses the BCrypt library once again.
            'Takes a password hash from the database, and a plaintext attempt (The user's password input) and compares them.
            'If the user's attempt matches the hash, return true. Else return false.
            Return Verify(attempt, hash)
        Catch
            'If something went wrong, we'll just default to false.
            Return False
        End Try
    End Function

    'Validate telephone number using Regex. Returns true if valid, else false.
    Function ValidateTelephone(ByVal telephoneNumber As String)
        Try
            Dim teleReg As Regex = New Regex("^(\+44)?\s?\(?0\)?[12358]\d\s?\d\)?\s?\d\)?(\s?\d){6}$")
            Return (teleReg.IsMatch(telephoneNumber))
        Catch
            Return False
        End Try
    End Function

    'Validate postcode using Regex. Returns true if valid, else false.
    Function ValidatePostcode(ByVal postCode As String)
        Try
            Dim postReg As Regex = New Regex("(((^[BEGLMNS][1-9]\d?)|(^W[2-9])|(^(A[BL]|B[ABDHLNRST]|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]|F[KY]|G[LUY]|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]|M[EKL]|N[EGNPRW]|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKL-PRSTWY]|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)\d\d?)|(^W1[A-HJKSTUW0-9])|(((^WC[1-2])|(^EC[1-4])|(^SW1))[ABEHMNPRVWXY]))(\s*)?([0-9][ABD-HJLNP-UW-Z]{2}))$|(^GIR\s?0AA$)")
            Return (postReg.IsMatch(postCode))
        Catch
            Return False
        End Try

    End Function

End Module

'Email Validation
'This is taken from Microsoft's MSDN website.
'http://msdn.microsoft.com/en-us/library/01escwtf(v=vs.110).aspx
'Uses a regular expression to validate the email.

Public Class RegexUtilities
    Dim invalid As Boolean = False

    Public Function IsValidEmail(strIn As String) As Boolean
        invalid = False
        If String.IsNullOrEmpty(strIn) Then Return False

        ' Use IdnMapping class to convert Unicode domain names. 
        Try
            strIn = Regex.Replace(strIn, "(@)(.+)$", AddressOf Me.DomainMapper,
                                  RegexOptions.None, TimeSpan.FromMilliseconds(200))
        Catch e As RegexMatchTimeoutException
            Return False
        End Try

        If invalid Then Return False

        ' Return true if strIn is in valid e-mail format. 
        Try
            Return Regex.IsMatch(strIn,
                   "^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                   "(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                   RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))
        Catch e As RegexMatchTimeoutException
            Return False
        End Try
    End Function

    Private Function DomainMapper(match As Match) As String
        Dim idn As New IdnMapping()

        Dim domainName As String = match.Groups(2).Value
        Try
            domainName = idn.GetAscii(domainName)
        Catch e As ArgumentException
            invalid = True
        End Try
        Return match.Groups(1).Value + domainName
    End Function
End Class
